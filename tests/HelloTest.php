<?php

use PHPUnit\Framework\TestCase;
use App\Hello;

class HelloTest extends TestCase
{
    public function testForSay()
    {
        $hello = new Hello();
        $this->assertEquals('Hello hash', $hello->say('hash'));
    }
}
